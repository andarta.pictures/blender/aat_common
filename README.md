# AAT_Common version 2.0

This repository centralises the common tools needed for every add-on in the Andarta Drawing Toolbox suite. 

## Version 2.0 changelog

Added files : 
 - import_utils
 - nijiPen_utils

Added functions : 
 - gp_utils.get_layer_matrix
 - geometry_3D.compose_matrix

Modified functions : 
 - utils.register_classes : now uses trycatch when registering to do intelligent print of errors.
 - gp_utils.is_collection_hierarchy_active : now better takes care of multi viewlayer cases into account

Deleted functions : 
 - Deletion of gp_utils.get_now_gpframe

## License

SPDX-License-Identifier: GPL-3.0-or-later
Copyright (C) 2023, Andarta Pictures. All rights reserved.
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright (C) 2023, Andarta Pictures. All rights reserved.

import bpy
import gpu
from gpu_extras.batch import batch_for_shader
import numpy as np
import mathutils
import random

from . import geometry_3D as geo3D

Log_3D = []
display = True
transparency = 0.5

class Log_3D_point :

    def __init__(self, coordinates, color, size) :
        self.coordinates = coordinates
        self.color = color
        self.size = size
        self.subscribe()
    
    def get_2D_position(self) :

        region = bpy.context.region
        r3d = bpy.context.space_data.region_3d

        return geo3D.location_to_region(self.coordinates, region, r3d)

    def get_poly_batch(self, n, start_angle=0.0) :
        pos_2D = self.get_2D_position()
        if pos_2D is None :
            return None
        pos_2D = np.array(pos_2D)
        radius = self.size

        batch = []

        for i in range(0, n+1) :
            angle_degrees = start_angle+360*i/n
            alpha = np.deg2rad(angle_degrees)
            v = np.array((radius*np.cos(alpha), radius*np.sin(alpha)))
            p = mathutils.Vector(pos_2D+v)
            batch.append(p)
        
        return batch

    def subscribe(self) :
        context = bpy.context
        args = (self, context)

        self.handler = bpy.types.SpaceView3D.draw_handler_add(self.draw, args, 'WINDOW', 'POST_PIXEL')

    def unsubscribe(self) :
        bpy.types.SpaceView3D.draw_handler_remove(self.handler, 'WINDOW')

    def draw(self, op, context):
        """Draw method"""

        global display

        shader = gpu.shader.from_builtin('UNIFORM_COLOR')
        gpu.state.blend_set('ALPHA')
        gpu.state.line_width_set(2.0)
        
        poly = self.get_poly_batch(4)
        
        if poly is not None and display :
            batch = batch_for_shader(shader, 'LINE_STRIP', {"pos": poly})
            shader.uniform_float("color", self.color)

            batch.draw(shader)

        # restore opengl defaults
        gpu.state.line_width_set(1.0)
        gpu.state.blend_set('NONE')
    

class Log_3D_line :

    def __init__(self, coordinates, color, size) :
        self.coordinates = coordinates
        self.color = color
        self.size = size
        self.subscribe()
    
    def get_2D_line(self) :
        region = bpy.context.region
        r3d = bpy.context.space_data.region_3d

        return [geo3D.location_to_region(c, region, r3d) for c in self.coordinates]

    def subscribe(self) :
        context = bpy.context
        args = (self, context)

        self.handler = bpy.types.SpaceView3D.draw_handler_add(self.draw, args, 'WINDOW', 'POST_PIXEL')

    def unsubscribe(self) :
        bpy.types.SpaceView3D.draw_handler_remove(self.handler, 'WINDOW')

    def draw(self, op, context):
        """Draw method"""

        global display

        shader = gpu.shader.from_builtin('UNIFORM_COLOR')
        gpu.state.blend_set('ALPHA')
        gpu.state.line_width_set(self.size)

        line = self.get_2D_line()
        valid_coordinates = all([p is not None for p in line])

        if valid_coordinates and display :
            batch = batch_for_shader(shader, 'LINE_STRIP', {"pos": line})
            shader.uniform_float("color", self.color)

            batch.draw(shader)

        # restore opengl defaults
        gpu.state.line_width_set(1.0)
        gpu.state.blend_set('NONE')


colors = {
    "GREEN": (0.5, 1.0, 0.4, 0.7),
    "ORANGE" : (0.9, 0.45, 0.0, 0.7),
    "RED" : (1.00,0.30,0.30, 0.7),
    "PURPLE" : (0.80,0.40,1.00, 0.7),
    "YELLOW" : (1.00,0.90,0.40, 0.7),
    "BLUE" : (0.40,0.50,1.00, 0.7),
    "PINK" : (1.00,0.50,1.00, 0.7),
}

available_colors = []


def get_random_color() :
    global colors, available_colors
    
    if len(available_colors) == 0 :
        available_colors = [c for c in colors.keys()]

    color = random.choice(available_colors)
    
    available_colors = [c for c in available_colors if c != color]
    return color


def get_color(color_name) :
    global transparency

    if color_name == "RANDOM" :
        color_name = get_random_color()
    if color_name in colors :
        r,g,b,a = colors[color_name]
        color = (r,g,b,transparency)
        return color
    else :
        return (0.5, 1.0, 0.4, transparency)
    
def destroy_3D_logs() :
    global Log_3D
    for e in Log_3D :
        e.unsubscribe()
    Log_3D = []

def point(coordinates, size=5.0, color_name="RANDOM") :
    point_3D = Log_3D_point(coordinates, get_color(color_name), size)
    Log_3D.append(point_3D)

def line(coordinates, size=2.0, color_name="RANDOM") :
    line_3D = Log_3D_line(coordinates, get_color(color_name), size)
    Log_3D.append(line_3D)

def set_display(display_status) :
    global display
    display = display_status

def get_display() :
    global display
    return display